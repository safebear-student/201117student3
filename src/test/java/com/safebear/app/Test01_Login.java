package com.safebear.app;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 20/11/2017.
 */
public class Test01_Login extends BaseTest {
    @Test
    public void testLogin() {
        //Step 1 - Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
        //Step 2 - Click on the login link and the Login Page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
        //Step 3 - Login with valid credentials
        assertTrue(loginPage.login(this.userPage, "testuser", "testing"));
        //Step 4 - Logout the page after 2 seconds
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertTrue(userPage.clickOnLogout(this.welcomePage));
    }
}
